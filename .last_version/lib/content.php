<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.push
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Push;

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Content {

	public static function get($arData){
		
		$arReturn = array();
		
		$event = new \Bitrix\Main\Event("mlife.push", "OnBeforeContentGet", array('data'=>$arData));
		$event->send();
		if ($event->getResults()){
			foreach($event->getResults() as $evenResult){
				if($evenResult->getResultType() == \Bitrix\Main\EventResult::SUCCESS){
					$ar = $evenResult->getParameters();
					if(!empty($ar)) $arReturn = $ar;
				}
			}
		}
		
		if(empty($arReturn)){
			$arReturn["mode"] = false;
		}
		
		if($arData['comand']=="update_chat" && intval($arData['chat'])>0){
			
			$arReturn["mode"] = "update_chat";
			$arReturn["id"] = intval($arData['chat']);
			$arReturn["url"] = "/personal/dialogs/".intval($arData['chat'])."/";
			
		}
		
		return \Bitrix\Main\Web\Json::encode($arReturn);
		
	}

}