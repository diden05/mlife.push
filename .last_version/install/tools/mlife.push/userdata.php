<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");


if(isset($_REQUEST["text"]) && is_string($_REQUEST["text"])){
	
	$data = explode("&",$_REQUEST["text"]);
	$dataAr = array();
	foreach($data as $key=>$val){
		if(strpos($val,"=")!==false){
			$val = explode("=",$val);
			if(count($val)==2){
				$dataAr[trim($val[0])] = trim($val[1]);
			}
		}
	}
	if(!empty($dataAr)){
		\Bitrix\Main\Loader::includeModule("mlife.push");
		echo \Mlife\Push\Content::get($dataAr);
		die();
	}
	
}

require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/epilog_after.php");
?>