<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MLIFE_PUSH_DEMO_C_NAME"),
	"DESCRIPTION" => GetMessage("MLIFE_PUSH_DEMO_C_DESC"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 30,
	"PATH" => array(
		"ID" => "mlife",
		"NAME" => GetMessage("MLIFE_NAME"),
		"CHILD" => array(
			"ID" => "mlife_dev",
			"NAME" => GetMessage("MLIFE_DEV"),
			"SORT" => 30,
		),
	),
);

?>