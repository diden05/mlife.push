create table if not exists `mlife_push_chanels` (
  `ID` varchar(60) NOT NULL,
  `USER` int(11) NOT NULL DEFAULT '0',
  `TIMEEND` int(11) NOT NULL,
  `TYPE` varchar(10) NOT NULL DEFAULT 'public',
  UNIQUE KEY `ID` (`ID`),
  KEY `USER` (`USER`),
  KEY `TIMEEND` (`TIMEEND`)
);